module.exports = (phase, { defaultConfig }) => {
  return {
    future: {
      webpack5: true,
    },
    poweredByHeader: false,
    pageExtensions: ['js', 'jsx'],
    target: 'serverless',
    dir: './src',
    compilerOptions: {
      baseUrl: ".",
      paths: {
        "@components/**": ["src/components/**"],
        "@pages/**": ["src/pages/**"]
      }
    },
  }
}