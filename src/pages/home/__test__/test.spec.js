import { parse, removeById } from '../actions'
import DATA from '../../../../public/mock/example-data'


describe('actions', () => {
  it('runs a test file', () => {
    expect(true).toBeTruthy()
  })

  it('parses correctly', () => {
    const newData = parse(DATA)
    expect(!!newData[0].children[0]).toBeTruthy()
  })

  it('removesById correctly', () => {
    let newData = [...parse(DATA)]
    newData[0].uuid = 'removeMe'
    const mutated = removeById('removeMe', newData)
    expect(mutated[0].uuid !== 'removeMe').toBeTruthy()
  })
})