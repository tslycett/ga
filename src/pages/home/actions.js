
import { v4 as uuid } from 'uuid';

export const get = (path, obj) => path
  .split('.')
  .reduce((acc, current) => acc && acc[current], obj);

export const parse = data => {
  return data.map(person => {
    person.uuid = uuid()
    if (get('kids.has_relatives.records', person)) {
      person.children = person.kids.has_relatives.records.map(child => {
        child.uuid = uuid();
        if (get('kids.has_phone.records', child)) {
          child.children = child.kids.has_phone.records.map(has_phone => {
            has_phone.uuid = uuid();
            return has_phone;
          })
        }
        return child
      })
    }
    return person
  })
}

export const removeById = (id, data) => {
  let newState = [];
  data.map(person => {
    if (person.uuid !== id) {
      if (person.children) {
        let newChild = [];
        person.children.map(child => {
          if (child.uuid !== id) {
            if (child.children) {
              child.children = child.children
                .filter(phone => phone.uuid === id)
            }
            newChild.push(child)
          }
        })
        person.children = newChild
      }
      newState.push(person)
    }
  });
  return newState
}
