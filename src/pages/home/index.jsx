import { useEffect, useState, Fragment } from 'react'
import { Container, Paper } from '@material-ui/core';
import { v4 as uuid } from 'uuid'
import { parse, removeById } from './actions'
import data from '../../../public/example-data.json'
import TestTable from './components/table'
const formattedData = parse(data)

export default function Home() {
  const [state, setState] = useState({
    id: uuid(),
    test: formattedData
  })

  const remove = uuid => {
    setState({ test: removeById(uuid, state.test) })
  }

  return (
    <Container maxWidth="lg">
      <Paper>
        <TestTable data={state.test} id={state.id} remove={remove} />
      </Paper>
    </Container>
  )
}
