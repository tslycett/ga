import { useState, Fragment } from 'react'
import { Collapse, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import { v4 as uuid } from 'uuid'
import Row from '../row'

export default function TaskTable({ data, remove }) {
  const [panel, setPanel] = useState(null);
  if (!data || !data[0]) {
    return null
  }

  return (
    <TableContainer>
      <Table aria-label="collapsible table">
        <TableHead style={{ background: 'silver' }}>
          <TableRow>
            <TableCell />
            {Object.keys(data[0].data).map(title =>
              <TableCell key={title}>{title}</TableCell>
            )}
            <TableCell />
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map(row =>
            <Fragment key={row.uuid}>
              <Row row={row} panel={panel} select={id => setPanel(id)} uid={row.uuid} remove={remove} />
              {row.children &&
                <TableRow>
                  <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={panel === row.uuid} timeout="auto">
                      <TaskTable data={row.children} remove={remove} />
                    </Collapse>
                  </TableCell>
                </TableRow>
              }
            </Fragment>
          )}
        </TableBody>
      </Table>
    </TableContainer>
  )
}

