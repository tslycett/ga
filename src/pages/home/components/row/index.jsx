import { IconButton, TableCell, TableRow } from '@material-ui/core';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';

export default function Row({ panel, select, row, uid, remove }) {
  return (
    <TableRow>
      {row.children ?
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={e => select(uid)}>
            {panel === uid ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        : <TableCell />
      }
      {Object.keys(row.data).map(title =>
        <TableCell key={title} component="th" scope="row">
          {row.data[title]}
        </TableCell>
      )}
      <TableCell><span onClick={e => remove(row.uuid)}>X</span></TableCell>
    </TableRow>
  )
}

