

assessment sample for graphaware.

> steps:

- initialized
- research
- add next + material + jest
- add layout
- add ui + comps
- add data layer
- wiring + adjustments
- tests


notes:

this was timeboxed, and does not do most things, 
Ideally. I would have liked to update the code a lot more

things that work:
- working base project
- basic render
- basic remove

things that don't
- logic is functional, not prod ready
- would add libs and better methodology around data mutation and management
- the collapse items need more logic 
- the remove needs more work

things I would still like to do if not timeboxed
- add proper store pattern
- more tests
- fix code