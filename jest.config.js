module.exports = {
  moduleNameMapper: {
    "^@components(.*)$": "<rootDir>/src/components$1",
    "^@pages(.*)$": "<rootDir>/pages$1"
  },
  testPathIgnorePatterns: ['/.next/', '/node_modules/', '/lib/', '/tests/', '/coverage/'],
  testRegex: '(/__test__/.*|\\.(test.spec))\\.(ts|tsx|js|jsx)$',
  testURL: 'http://localhost',
  testEnvironment: 'jsdom',
  moduleFileExtensions: ['js', 'jsx', 'json'],
  transform: {
    '.(js|jsx|json)': 'babel-jest',
  },
  transformIgnorePatterns: ['<rootDir>/node_modules/'],
};